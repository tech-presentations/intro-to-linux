#!/usr/bin/env bash

set -x
set -e

if [[ ! -e public ]]; then
    mkdir public
fi

cd public

if [[ ! -e reveal.js ]]; then
    wget https://github.com/hakimel/reveal.js/archive/master.tar.gz
    tar -xzvf master.tar.gz
    rm -f master.tar.gz
    mv reveal.js-master reveal.js
fi

THEME=solarized

for FULL_FILE in $(ls ../*.md); do

    BASE_FILE=$(basename ${FULL_FILE})
    LABEL=$(echo ${BASE_FILE%.*})


    if [[ ! -e ${LABEL} ]]; then
        mkdir ${LABEL}
    fi

    pandoc -t revealjs \
        --verbose \
        -s ${FULL_FILE} \
        --toc \
        -o ${LABEL}/index.html \
        -V theme=${THEME} \
        -V revealjs-url=../reveal.js
    
    pandoc --verbose \
        -s ${FULL_FILE} \
        -V theme=${THEME} \
        -o ${LABEL}/index.pdf || true
    
done
