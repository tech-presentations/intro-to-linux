---
author: Drew Stinnett
title: Introduction to Linux Addendum
lang: en
---

## VIM Basics

### Opening a File

```
$ vim filename.txt
```

`Command Mode` - Powerful navigation and editing in a document, need to press `escape` when in `Insert Mode`

`Insert Mode` - 'i' - Insert actual text

`hjkl` - Navigation in the document without fingers leaving home row

---

### Closing a File

All of the following commands require `Command Mode`

`:wq` - Write then Quit

`:q` - Quit

`:q!` - Quit, aborting any unsaved changes

# Git Basics

Modern Version Control System, keep track of changes made, and who made them.

Basic Usage:

```
$ git clone

$ git status

$ git commit

$ git pull

$ git push

```

---

Advanced Usage

```
$ git branch

$ git merge
```


## Shell Scripting Basics

Chain commands together and save them in a script.  This makes for easily reproducable and distributable workflows

All shell scripts need a first line in order to know which shell to execute.  These look like this;

`#!/bin/bash`

---

### From the Shell

```bash
cd /tmp

git clone https://github.com/GothenburgBitFactory/taskwarrior.git

cd taskwarrior

head NEWS
```

---

- Make this a script with: `vim taskwarrior_news.sh`

- Enter Insert mode with `i`

- Insert the shebang line

- Paste in commands

- Command mode and `:wq`

- Make the script executable with `chmod u+x taskwarrior_news.sh`

- Execute script with `./taskwarrior_news.sh`

## How would we make this better?

::: notes

Logging, maybe use logger

Does git even exist?

Check for errors...did TaskWarrior actually download?

Does the NEWS File exist?

:::

--- 

## Python Scripting

- Object oriented

- Supported on most OSes

- Easy to prototype simple and complex applications

- Lots of useful 3rd party extensions

---

### Simple Python

```python
#!/usr/bin/python

def say_hi():
    print("Hello world")


def say_bye():
    print("Goodbye world")


say_hi()

math_result = 5 * 42

print("Our math example result is: %s" % math_result)

say_bye()
```

---

### Slightly Better Python

```python
#!/usr/bin/python
import sys


def say_something(words):
    print(words)


def main():
    say_something("Hello world")

    math_result = 5 * 42

    say_something("Our math example result is: %s" % math_result)

    say_something("Goodbye world")


if __name__ == "__main__":
    sys.exit(main())
```

## General Best Practices

- Log as much as you can

- Check for errors, don't assume things will always work the same way as when you wrote the script

- Run linters on your code

- Use version control like `git`
