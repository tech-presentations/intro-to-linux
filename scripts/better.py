#!/usr/bin/python
import sys


def say_something(words):
    print(words)


def main():
    say_something("Hello world")

    math_result = 5 * 42

    say_something("Our math example result is: %s" % math_result)

    say_something("Goodbye world")


if __name__ == "__main__":
    sys.exit(main())
