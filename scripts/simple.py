#!/usr/bin/python


def say_hi():
    print("Hello world")


def say_bye():
    print("Goodbye world")


say_hi()

math_result = 5 * 42

print("Our math example result is: %s" % math_result)

say_bye()
